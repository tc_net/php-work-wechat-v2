<?php
/**
 * Created by phpstorm.
 * Author：tc-net
 * Date: 2019/10/14
 * Time: 14:35
 */

namespace Tcnet\Workwx;

use Tcnet\Workwx\client\CustomerClient;
use Tcnet\Workwx\client\DepartmentClient;
use Tcnet\Workwx\client\MessageClient;
use Tcnet\Workwx\client\QrLoginClient;
use Tcnet\Workwx\client\UserClient;


/**
 * 入口文件
 *
 * Class WorkwxClient
 * @package Tcnet\Workwx
 */
class WorkWxClient
{
    /**
     * @var
     */
    protected static $userClient;

    /**
     * @var
     */
    protected static $qrLoginClient;

    /**
     * @var
     */
    protected static $messageClient;

    /**
     * @var
     */
    protected static $departmentClient;

    protected static $customerClient;
    /**
     * 企业id
     *
     * @var string
     */
    protected static $corpId = '';

    /**
     * 应用凭证秘钥
     *
     * @var string
     */
    protected static $secret = '';

    /**
     * 授权方的网页应用ID
     * @var string
     */
    protected static $agentId;

    /**
     * 实例化
     * Author：tc-net
     * Date: 2019/10/14
     * Time: 14:56
     *
     * @param $corpId
     * @param $secret
     * @param $agentId
     * @return WorkWxClient
     */
    public static function initialize($corpId, $secret, $agentId=null)
    {
        self::$corpId = $corpId;
        self::$secret = $secret;
        self::$agentId = $agentId;

        return new self();
    }

    /**
     * 实例化用户接口
     * Author：tc-net
     * Date: 2019/10/14
     * Time: 14:42
     *
     * @return UserClient
     * @throws exception\WorkWxExcetion
     */
    public function userClient()
    {
        if (!is_object(self::$userClient)) {
            self::$userClient = new UserClient(self::$corpId, self::$secret, self::$agentId);
        }

        return self::$userClient;
    }


    /**
     * 登录实例
     *
     * Author：tc-net
     * Date: 2019/10/14
     * Time: 14:47
     *
     * @return QrLoginClient
     * @throws exception\WorkWxExcetion
     */
    public function qrLoginClient()
    {
        if (!is_object(self::$qrLoginClient)) {
            self::$qrLoginClient = new QrLoginClient(self::$corpId, self::$secret, self::$agentId);
        }

        return self::$qrLoginClient;
    }

    /**
     * 通知实例
     *
     * Author：tc-net
     * Date: 2019/10/14
     * Time: 14:47
     *
     * @return MessageClient
     * @throws exception\WorkWxExcetion
     */
    public function messageClient()
    {
        if (!is_object(self::$messageClient)) {
            self::$messageClient = new MessageClient(self::$corpId, self::$secret, self::$agentId);
        }

        return self::$messageClient;
    }

    /**
     * 部门实例
     *
     * Author：tc-net
     * Date: 2019/10/17
     * Time: 9:32
     *
     * @return DepartmentClient
     * @throws exception\WorkWxExcetion
     */
    public function departmentClient()
    {
        if (!is_object(self::$departmentClient)) {
            self::$departmentClient = new DepartmentClient(self::$corpId, self::$secret, self::$agentId);
        }

        return self::$departmentClient;
    }

    public function customerClient()
    {
        if (!is_object(self::$customerClient)) {
            self::$customerClient = new CustomerClient(self::$corpId, self::$secret, self::$agentId);
        }

        return self::$customerClient;
    }
}
