<?php
/**
 * Created by phpstorm.
 * Author：tc-net
 * Date: 2019/10/12
 * Time: 18:21
 */

namespace Tcnet\Workwx\client;


use Tcnet\Workwx\tool\HttpClient;
use Tcnet\Workwx\WorkWxBase;

/**
 * 客户
 *
 * @package Tcnet\Workwx\client
 */
class CustomerClient extends WorkWxBase
{
    public function __construct($corpId = '', $secret = '', $agentId = '')
    {
        parent::__construct($corpId, $secret, $agentId);
    }

    /**
     * 获取配置了客户联系功能的成员列表
     * @param $token
     * @return array|mixed
     * @throws \Tcnet\Workwx\exception\WorkWxApiExcetion
     */
    public function getFollowUserList($token)
    {

        $param['access_token'] = $token;
        $url = $this->baseUrl.'/externalcontact/get_follow_user_list?'.http_build_query($param);

        $data = HttpClient::initialize($url)->get();

        return $data;
    }

    /**
     * 获取客户列表
     * @param $token 	调用接口凭证
     * @param $userid 	企业成员的userid
     * @return array|mixed
     * @throws \Tcnet\Workwx\exception\WorkWxApiExcetion
     */
    public function getExternalcontactList($token, $userid)
    {
        $param['access_token']  = $token;
        $param['userid']        = $userid;
        $url = $this->baseUrl . '/externalcontact/list?'.http_build_query($param);
        $data = HttpClient::initialize($url)->get();

        return $data;
    }

    /**
     * 获取客户详情
     * @param $token 	调用接口凭证
     * @param $external_userid 	外部联系人的userid，注意不是企业成员的帐号
     * @return array|mixed
     * @throws \Tcnet\Workwx\exception\WorkWxApiExcetion
     */
    public function getExternalcontactInfo($token, $external_userid,$cursor=null)
    {
        $param['access_token']          = $token;
        $param['external_userid']       = $external_userid;
        if(!empty($cursor)) $param['cursor'] = $cursor;
        $url = $this->baseUrl . '/externalcontact/get?'.http_build_query($param);
        $data = HttpClient::initialize($url)->get();

        return $data;
    }
}
