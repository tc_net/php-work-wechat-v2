<?php
/**
 * Created by phpstorm.
 * Author：tc-net
 * Date: 2019/10/12
 * Time: 14:10
 */

namespace Tcnet\Workwx\client;

use Tcnet\Workwx\exception\ErrorCode;
use Tcnet\Workwx\exception\WorkWxExcetion;
use Tcnet\Workwx\tool\HttpClient;
use Tcnet\Workwx\WorkWxBase;

/**
 * 用户
 * Class UserClient
 * @package Tcnet\Workwx\client
 */
class UserClient extends WorkWxBase
{
    /**
     * UserClient constructor.
     * @param string $corpId
     * @param string $secret
     * @param string $agentId
     * @return array
     *@throws \Tcnet\Workwx\exception\WorkWxExcetion
     */
    public function __construct($corpId = '', $secret = '', $agentId = '')
    {
        parent::__construct($corpId, $secret, $agentId);
    }

    /**
     * 获取用户信息
     *
     * Author：tc-net
     * Date: 2019/10/17
     * Time: 16:35
     *
     * @param $accessToken
     * @param $code
     * @return mixed
     * @throws WorkWxExcetion
     * @throws \Tcnet\Workwx\exception\WorkWxApiExcetion
     */
    public function getUserInfo($accessToken, $code)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token={$accessToken}&code={$code}";

        $data =  HttpClient::initialize($url)->get();

        if (!empty($data['OpenId'])) {
            throw new WorkWxExcetion('非企业内部人员', ErrorCode::PERMISSION_DENIED);
        }

        return $data;
    }
    public function getUser($accessToken, $userid)
    {
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={$accessToken}&userid={$userid}";

        $data =  HttpClient::initialize($url)->get();
        return $data;
    }

    /**
     * @param $accessToken
     * @param $mobile
     * @return array
     * @throws \Tcnet\Workwx\exception\WorkWxApiExcetion
     */
    public function getMobileUserid($accessToken,$mobile){
        $url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserid?access_token={$accessToken}";
        //mobile
        $data =  HttpClient::initialize($url)->post(["mobile"=>$mobile]);
        return $data;
    }
}
