<?php
/**
 * Created by phpstorm.
 * Author：tc-net
 * Date: 2019/10/12
 * Time: 14:14
 */
namespace Tcnet\Workwx\exception;

/**
 * 微信官方接口异常
 * Class WorkwxApiExcetion
 * @package Tcnet\Workwx\exception
 */
class WorkWxApiExcetion extends \Exception
{

}